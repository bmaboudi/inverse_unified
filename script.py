import numpy as np
#import gauss_hermit as gh
import high_fidelity as hf
import metropolis as metro
import nn_network

problem_type = 'blob'
ndim = 5

model_params = {}
model_params['gaus_rand_field'] = np.random.normal(0,1,ndim)
field_mean = np.zeros(2)
field_mean[0] = 2.
field_mean[1] = 4.
model_params['gaus_mean'] = field_mean

model_params['blob_center'] = np.random.uniform(3/8,5/8,2)
model_params['off_circle'] = np.random.uniform(1/8,1/4,1)

high_fid = hf.high_fidelity(problem_type)
xs = np.zeros(2)
xs[0] = 0.5
xs[1] = 0.5
high_fid.set_source_loc(xs)
high_fid.assemble_right_hand_side()
high_fid.set_params(model_params)
high_fid.assemble_bilinear_matrix()
y = high_fid.solve()

print([ model_params['blob_center'] , model_params['off_circle'] ])

mu = 0.0
sigma = 0.05

y_observed = y

quad_num_nodes_1d = 5

mtr = metro.Metropolis(y_observed, sigma)
eval_func = nn_network.network_eval('./model.pt')
mtr.set_gauss_hermit_params(ndim, 'sparse_grid', eval_func.eval_model,level=2, problem_type='blob')

mean_vec = np.zeros((10,3))

for i in range(0,10):
    print('iteration :', (i+1))
    mtr.metro_alg()
    mean = mtr.compute_mean()
    mean_vec[i,:] = mean
#
#
file_name = 'result.npz'
np.savez(file_name,line_params=line_params, mean_vec=mean_vec)
#
print(mean)
