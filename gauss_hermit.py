import numpy as np
import scipy.io
import progressbar
import data_generation as data
import nn_network

class gauss_hermit_integration():
    def __init__(self, ndim, mode='full_grid', num_nodes_1d=None, level=None, path='./precomputed', problem_type='blob'):
        self.data = data.data(0,0,'blob')
        if(mode=='full_grid'):
            print('Initiating Gauss-Hermite quadrature with a full grid...')
            if(num_nodes_1d==None):
                print('Error: the number of 1D quadrature points must be specified!')
                exit()

            x, w = np.polynomial.hermite.hermgauss(num_nodes_1d)

            x = np.transpose( np.matrix(x) )
            w = np.transpose( np.matrix(w) )

            self.nodes = x
            self.weights = w

            for i in range(0, ndim-1):
                ones_L = np.ones( [self.nodes.shape[0], 1] )
                ones_R = np.ones( [x.shape[0], 1] )
                v1 = np.kron( self.nodes, ones_R )
                v2 = np.kron( ones_L, x )
                self.nodes = np.concatenate( (v1,v2), axis=1 )
                self.weights = np.kron(self.weights, w)

            adjust = 1 / (np.sqrt( np.pi )**ndim)
            self.weights = np.squeeze( np.asarray(self.weights) )
            self.weights = self.weights * adjust

        if(mode=='sparse_grid'):
            print('Initiating Gauss-Hermite quadrature with a sparse grid...')
            if(level==None):
                print('Error: the level of sparsity of the grid must be specified!')
                exit()

            file = path + '/sg_gh_' + str(ndim) + '_' + str(level) + '.mat'
            mat_file = scipy.io.loadmat(file)
            self.weights = np.squeeze( np.asarray( mat_file['gh_weights'] ) )
            self.nodes = np.transpose( mat_file['gh_nodes'] )

        adjust = 1 / (np.sqrt( np.pi )**ndim)
        self.weights = self.weights * adjust

    def set_quad_func(self, func):
        self.func = func

    def num_nodes(self):
        return self.nodes.shape[0]

    def get_node(self, idx):
        if( idx > self.nodes.shape[0]-1 ):
            print('index out of bound!')
            exit()
        return np.squeeze( np.asarray( self.nodes[idx,:]) ), np.squeeze( np.asarray( self.weights[idx] ) )

    def marginalize(self, y_observed, theta, sigma2):
        f = []

        for i in range(0,self.weights.size):

            gaus_vec = np.squeeze( np.asarray(self.nodes[i,:]) )
            params = self.data.construct_model_params(gaus_vec, theta)

            y = self.func(params)

            value = np.exp( - np.inner(y - y_observed, y - y_observed)/2/sigma2)#/np.power( np.sqrt( 2*np.pi*sigma2 ) , dim_f )
            f.append( value )

        return np.sum( np.multiply(f,self.weights) )

def dummy_func(x):
    return 1

if __name__ == '__main__':
    gh = gauss_hermit_integration(5,mode='sparse_grid',level=3)

    eval_func = nn_network.network_eval('./model.pt')
    gh.set_quad_func( eval_func.eval_model )

    gh.marginalize(1,[1,1,1],0.05)
