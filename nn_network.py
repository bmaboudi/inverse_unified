import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import argparse
import progressbar
import data_generation as data
import os.path

class network(nn.Module):
    def __init__( self, input_size, output_size ):
        super().__init__()
        self.fc1 = nn.Linear(input_size, 15)
        self.fc2 = nn.Linear(15, 30)
        self.fc3 = nn.Linear(30, 45)
        self.fc4 = nn.Linear(45, output_size)

    def forward( self, x ):
        x = torch.tanh( self.fc1(x) )
        x = torch.tanh( self.fc2(x) )
        x = torch.tanh( self.fc3(x) )
        return self.fc4(x)

def train_model(model_path, sample_size, n_field_expansion, nn_input_size, nn_output_size):
    path = 'train_data.npz'
    train_data = data.data(500, 5, 'blob')
    train_data.load_data(path)

    path = 'test_data.npz'
    test_data = data.data(500, 5, 'blob')
    test_data.load_data(path)

    model = network( nn_input_size, nn_output_size )

    if( os.path.isfile(model_path) == True ):
        model.load_state_dict(torch.load(model_path))

    device = torch.device('cpu')
    optimizer = optim.SGD(model.parameters(), lr = 0.001)

    max_iter = 400

    new_data = data.data(sample_size, 5, 'blob')
    for iter in range(0,400):
        new_data.new_batch()
        new_data.to_troch()

        print('training neural network: ', iter)
        for i in range( 0, max_iter ):
            model.train()
            for j in range( 0, len( train_data.in_data ) ):
                optimizer.zero_grad()

                y_ = model( train_data.torch_in[j] )

                y = train_data.torch_out[j]

                loss = torch.norm(y - y_, 2)
                loss.backward()

                optimizer.step()

            for j in range( 0, len( new_data.in_data ) ):
                optimizer.zero_grad()

                y_ = model( new_data.torch_in[j] )

                y = new_data.torch_out[j]

                loss = torch.norm(y - y_, 2)
                loss.backward()

                optimizer.step()

            model.eval()
            error_vec = []
            for j in range(0, len( test_data.in_data ) ):
                y_ = model( test_data.torch_in[j] )

                y = test_data.torch_out[j]
                error_vec.append( torch.norm(y_ - y, 2).detach().numpy() )
            print([i, np.min(error_vec), np.mean(error_vec), np.max(error_vec)])

        torch.save( model.state_dict(), model_path )

class network_eval():
    def __init__(self, model_path):
        self.model = network( 8, 66 )
        self.model.load_state_dict(torch.load(model_path))
        self.model.eval()

    def eval_model(self, in_vec):
        out_vec = self.model( in_vec )
        return out_vec.detach().numpy()

def parse_commandline():
    parser = argparse.ArgumentParser(description='Decoder')
    parser.add_argument('-o','--operation',help='Operation', required=True)
    parser.add_argument('-mp','--model-path',help='Model Path', required=True)

    args = parser.parse_args()
    return args

if __name__ == '__main__':
    sample_size = 500
    n_field_expansion = 5

    nn_input_size = n_field_expansion + 3
    nn_output_size = 66

    args = parse_commandline()

    if args.operation == 'train':
        train_model(args.model_path, sample_size, n_field_expansion, nn_input_size, nn_output_size)
