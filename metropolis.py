import numpy as np
import gauss_hermit
import high_fidelity as hf
import progressbar

class Metropolis:
	def __init__(self, y_observed, sigma):
		self.y_observed = y_observed
		self.sigma2 = sigma*sigma
		self.epsilon = 2.22044604925e-16
		self.N = 50000

	def set_gauss_hermit_params(self, ndim, mode, eval_func, num_nodes_1d=None, level=None, path='./precomputed', problem_type=None):
		if( problem_type == 'blob' ):
			self.gh = gauss_hermit.gauss_hermit_integration(ndim, mode=mode,level=level, problem_type=problem_type)
			self.gh.set_quad_func( eval_func )

	def density_ratio(self, x, y):
		ex = self.gh.marginalize(self.y_observed, x , self.sigma2)
		ey = self.gh.marginalize(self.y_observed, y , self.sigma2)

		return np.min([1.0, ey/ex])

	def metro_alg(self):
		x = np.zeros(3)
		x[0] = np.random.uniform(3/8,5/8)
		x[1] = np.random.uniform(3/8,5/8)
		x[2] = np.random.uniform(1/8,1/4)
		self.X = x

#		for i in range(0,self.N):
		for i in progressbar.progressbar(range(0, self.N)):
			y = np.random.uniform(0,1,3)
			y[0] = x[0] + 0.02*( y[0] - 0.5 )
			y[1] = x[1] + 0.02*( y[1] - 0.5 )
			y[2] = x[2] + 0.01*( y[2] - 0.5 )
			if( y[0]<3/8 or y[0]>5/8 or y[1]<3/8 or y[1]>5/8 or y[2]<1/8 or y[2]>1/4  ):
				self.X = np.append(self.X,x)
			else:
				alpha = self.density_ratio(x, y)
				u = np.random.uniform(0,1)
				if( u < alpha ):
					x = y
					self.X = np.append(self.X,y)
				else:
					self.X = np.append(self.X,x)

	def compute_mean(self):
		samples = self.X.reshape(self.N+1,3)
		mean = np.sum(samples,axis = 0)
		return mean/(self.N + 1)
