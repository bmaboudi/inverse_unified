import numpy as np
import high_fidelity as hf
import torch
import progressbar

class data():
    def __init__(self, num_data, num_kl_terms, action):
        self.problem_type = action
        self.num_kl_terms = num_kl_terms
        self.num_data = num_data

        self.in_data = []
        self.out_data = []

        self.torch_in = []
        self.torch_out = []

        self.high_fid = hf.high_fidelity(action)
        xs = np.zeros(2)
        xs[0] = 0.5
        xs[1] = 0.5
        self.high_fid.set_source_loc(xs)
        self.high_fid.assemble_right_hand_side()

    def generate_model_params(self):
        if(self.problem_type == 'blob'):
            model_params = {}
            model_params['gaus_rand_field'] = np.random.normal(0,1,self.num_kl_terms)
            field_mean = np.zeros(2)
            field_mean[0] = 2.
            field_mean[1] = 4.
            model_params['gaus_mean'] = field_mean

            model_params['blob_center'] = np.random.uniform(3/8,5/8,2)
            model_params['off_circle'] = np.random.uniform(1/8,1/4,1)

        return model_params

    def construct_model_params(self, gaus_rand_field, theta):
        if(self.problem_type == 'blob'):
            model_params = {}
            model_params['gaus_rand_field'] = gaus_rand_field
            field_mean = np.zeros(2)
            field_mean[0] = 2.
            field_mean[1] = 4.
            model_params['gaus_mean'] = field_mean
            model_params['blob_center'] = theta[0:2]
            model_params['off_circle'] = [ theta[2] ]

        input_vec = self.give_param_vec(model_params)
        return torch.from_numpy( input_vec ).float()

    def give_param_vec(self, model_params):
        model_params.pop('gaus_mean', None)
        list = []
        for i in model_params:
            list.append( np.array( model_params[i] ) )

        return np.concatenate(list,axis=0)


    def new_batch(self):
        self.in_data.clear()
        self.out_data.clear()

        print('generating data...')
        for i in progressbar.progressbar(range(0, self.num_data)):
#        for i in range(0,self.num_data):
            model_params = self.generate_model_params()
            self.high_fid.set_params(model_params)
            self.high_fid.assemble_bilinear_matrix()
            output = self.high_fid.solve()

            input = self.give_param_vec(model_params)

            self.in_data.append( input )
            self.out_data.append( output )

    def to_troch(self):
        self.torch_in.clear()
        self.torch_out.clear()

        for i in range(0,self.num_data):
            self.torch_in.append( torch.from_numpy(self.in_data[i] ).float() )
            self.torch_out.append( torch.from_numpy(self.out_data[i] ).float() )

    def generate_train_test_data(self):
        self.new_batch()
        filename = 'train_data.npz'
        np.savez(filename, in_data=self.in_data, out_data=self.out_data)

        self.new_batch()
        filename = 'test_data.npz'
        np.savez(filename, in_data=self.in_data, out_data=self.out_data)

    def load_data(self, data_path):
        file = np.load(data_path)
        in_data = file['in_data']
        out_data = file['out_data']

        self.in_data.clear()
        self.out_data.clear()

        self.out_data = out_data
        self.in_data = in_data

        self.to_troch()

if __name__ == '__main__':
    d = data(500, 5, 'blob')
#    d.generate_train_test_data()

    d.load_data('train_data.npz')
    print(len( d.torch_in) )
