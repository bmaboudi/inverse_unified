import numpy as np
from dolfin import *
import argparse

set_log_level(50)

class random_field_continuous(UserExpression):
	def set_params(self, model_params):
		self.field_mean = model_params['gaus_mean']
		self.field_params = model_params['gaus_rand_field']
		self.num_field_params = self.field_params.size

	def eval(self, values, x):
		values[0] = self.field_mean
		for i in range(0,self.num_field_params):
			values[0] += exp(-np.pi*(i+1)*0.1) * self.field_params[i] * np.sin(np.pi*i*x[0]) * np.sin(np.pi*i*x[1])

class random_field_one_line(UserExpression):
	def set_params(self, model_params):
		self.field_mean = model_params['gaus_mean']
		self.field_params = model_params['gaus_rand_field']
		self.num_field_params = self.field_params.size

		line_params = model_params['line_params']
		self.line = np.zeros(2)
		self.line[0] = line_params[0] - line_params[1]
		self.line[1] = -line_params[0]

	def eval(self, values, x):
		if ( x[1] + self.line[0]*x[0] + self.line[1] > 0 ):
			values[0] = self.field_mean[0]
			for i in range(0,self.num_field_params):
				values[0] += 0.1*exp(-np.pi*(i+1)*0.1) * self.field_params[i] * np.sin(np.pi*(i+1)*x[0]) * np.sin(np.pi*(i+1)*x[1])
		else:
			values[0] = self.field_mean[1]
			for i in range(0,self.num_field_params):
				values[0] += 0.1*exp(-np.pi*(i+1)*0.1) * self.field_params[i] * np.sin(np.pi*(i+1)*x[0]) * np.sin(np.pi*(i+1)*x[1])

class random_field_two_lines(UserExpression):
	def set_params(self, model_params):
		self.field_mean = model_params['gaus_mean']
		self.field_params = model_params['gaus_rand_field']
		self.num_field_params = self.field_params.size

		line_params = model_params['line_params']
		self.line1 = np.zeros(2)
		self.line1[0] = line_params[0] - line_params[1]
		self.line1[1] = -line_params[0]
		self.line2 = np.zeros(2)
		self.line2[0] = line_params[2] - line_params[3]
		self.line2[1] = -line_params[2]

	def eval(self, values, x):
		if( (x[1] + self.line1[0]*x[0] + self.line1[1] > 0) and (x[1] + self.line2[0]*x[0] + self.line2[1] > 0) ):
			values[0] = self.field_mean[0]
			for i in range(0,self.num_field_params):
				values[0] += 0.1*exp(-np.pi*(i+1)*0.1) * self.field_params[i] * np.sin(np.pi*(i+1)*x[0]) * np.sin(np.pi*(i+1)*x[1])
		elif( x[1] + self.line1[0]*x[0] + self.line1[1] > 0 and x[1] + self.line2[0]*x[0] + self.line2[1] < 0 ):
			values[0] = self.field_mean[1]
			for i in range(0,self.num_field_params):
				values[0] += 0.1*exp(-np.pi*(i+1)*0.1) * self.field_params[i] * np.sin(np.pi*(i+1)*x[0]) * np.sin(np.pi*(i+1)*x[1])
		elif( x[1] + self.line1[0]*x[0] + self.line1[1] < 0 and x[1] + self.line2[0]*x[0] + self.line2[1] > 0 ):
			values[0] = self.field_mean[2]
			for i in range(0,self.num_field_params):
				values[0] += 0.1*exp(-np.pi*(i+1)*0.1) * self.field_params[i] * np.sin(np.pi*(i+1)*x[0]) * np.sin(np.pi*(i+1)*x[1])
		else:
			values[0] = self.field_mean[3]
			for i in range(0,self.num_field_params):
				values[0] += 0.1*exp(-np.pi*(i+1)*0.1) * self.field_params[i] * np.sin(np.pi*(i+1)*x[0]) * np.sin(np.pi*(i+1)*x[1])

class random_field_blob(UserExpression):
	def set_params(self, model_params):
		self.field_mean = model_params['gaus_mean']
		self.field_params = model_params['gaus_rand_field']
		self.num_field_params = self.field_params.size
		self.center = model_params['blob_center']
		self.off_circle = model_params['off_circle']

	def eval(self, values, x):
		if ( (x[0] - self.center[0])**2/(1/16) + (x[1] - self.center[1])**2/self.off_circle**2 > 1 ):
			values[0] = self.field_mean[0]
			for i in range(0,self.num_field_params):
				values[0] += 0.1*exp(-np.pi*(i+1)*0.1) * self.field_params[i] * np.sin(np.pi*(i+1)*x[0]) * np.sin(np.pi*(i+1)*x[1])
		else:
			values[0] = self.field_mean[1]
			for i in range(0,self.num_field_params):
				values[0] += 0.1*exp(-np.pi*(i+1)*0.1) * self.field_params[i] * np.sin(np.pi*(i+1)*x[0]) * np.sin(np.pi*(i+1)*x[1])

class source_term(UserExpression):
	def set_source_loc(self, xc):
		self.xc = xc
	def eval(self, values, x):
		values[0] = 50*exp(-(pow(x[0] - self.xc[0] , 2) + pow(x[1] - self.xc[1] , 2)) / 0.1)

def boundary(x):
	return x[0] < DOLFIN_EPS or x[0] > 1.0 - DOLFIN_EPS

def output_indices(x):
	return x[1] < DOLFIN_EPS or x[1] > 1.0 - DOLFIN_EPS

class high_fidelity():
	def __init__(self, action):
		self.action = action
		self.mesh = UnitSquareMesh(32,32)
		self.V = FunctionSpace(self.mesh, 'Lagrange', 1)
		FEM_el = self.V.ufl_element()

		#		self.alpha = random_field_sin(element = FEM_el)
		#		self.alpha = random_field_sin_single(element = FEM_el)
		#		self.alpha = random_field_discontinuous(element = FEM_el)
		#		self.alpha = random_field_sin_discontinuous(element = FEM_el)
		#		self.alpha = random_field_sin_continuous(element = FEM_el)
		#		self.alpha = random_field_blob(element = FEM_el)
		if(self.action == 'two_lines'):
			self.alpha = random_field_two_lines(element = FEM_el)
		elif(self.action == 'line'):
			self.alpha = random_field_one_line(element = FEM_el)
		elif(self.action == 'blob'):
			self.alpha = random_field_blob(element = FEM_el)
		elif(self.action == 'continuous'):
			self.alpha = random_field_continuous(element = FEM_el)
		else:
			print('incorrect action!')
			exit()
		self.f = source_term(element = FEM_el)
		#		self.g = Expression("sin(5*x[0])", degree=2)
		self.g = Constant(0.0)

		self.u0 = Constant(0.0)
		self.bc = DirichletBC(self.V, self.u0, boundary)

		self.u = TrialFunction(self.V)
		self.v = TestFunction(self.V)

		dummy = Function(self.V)
		constant_dummy = Constant(1.0)
		bc_dummy = DirichletBC(self.V, 1, output_indices)
		bc_dummy.apply( dummy.vector() )
		vec_dummy = np.squeeze( np.asarray( np.matrix( dummy.vector() ) ) )
		idx = np.where(vec_dummy == 1)
		idx = np.squeeze( np.asarray(idx) )
		self.idx_selector = np.zeros( [ idx.size, np.squeeze( np.asarray( dummy.vector() ) ).size ])
		for i in range(0,idx.size):
			self.idx_selector[i,idx[i]] = 1

	def set_params(self, model_params):
		self.alpha.set_params(model_params)

	def set_source_loc(self, source_loc):
		self.f.set_source_loc(source_loc)

	def give_bilinear_matrix(self, idx):
		self.alpha.set_param_index(idx)
		a = inner( self.alpha*grad(self.u) , grad(self.v) )*dx
		A = assemble(a)
		return np.matrix(A.array())

	def assemble_bilinear_matrix(self):
		a = inner( self.alpha*grad(self.u) , grad(self.v) )*dx
		A = assemble(a)
		self.bc.apply(A)
		self.A_mat = np.matrix(A.array())

	def assemble_right_hand_side(self):
		L = self.f*self.v*dx + self.g*self.v*ds
		b = assemble(L)
		self.bc.apply(b)
		self.b_vec = np.transpose( np.matrix(b.get_local()) )

	def solve(self):
		self.solution = np.linalg.solve(self.A_mat,self.b_vec)
		return_vec = self.idx_selector * self.solution
		return np.matrix( np.squeeze( np.asarray(return_vec) ) )

	def save_solution(self):
		u = Function(self.V)
		u.vector().set_local( self.solution )
		file = File('solution.pvd')
		file << u

	def save_random_field(self):
		alpha_func = interpolate(self.alpha, self.V)
		file = File('random_field.pvd')
		file << alpha_func

	def save_from_vec(self, u_vec):
		u = Function(self.V)
		u.vector().set_local( np.squeeze( np.asarray( u_vec ) ) )
		file = File('from_vec.pvd')
		file << u

	def compute_error_vec(self, v):
		e = Function(self.V)
		e.vector().set_local( v )
		error = assemble(e**2*dx)
		print(error)

	def script(self):
		f = Function(self.V)
		f = self.alpha.set_param_index(-1)
		for i in range(0, self.num_param_expan):
			f = f + self.alpha.set_param_index(i)
#		self.save_random_field()

def generate_model_params():
	problem_params = np.zeros(4)
	problem_params[0] = np.random.uniform(0.1,0.9)
	dummy = np.random.uniform(0.1,0.9)
	while( np.abs( problem_params[0] - dummy ) < 0.1 ):
		dummy = np.random.uniform(0.1,0.9)
	problem_params[1] = dummy

	problem_params[2] = np.random.uniform(0.1,0.9)
	dummy = np.random.uniform(0.1,0.9)
	while( np.abs( problem_params[2] - dummy ) < 0.1 ):
		dummy = np.random.uniform(0.1,0.9)
	problem_params[3] = dummy

	temp = np.zeros(4)
	temp[0] = np.min( problem_params[0:2] )
	temp[1] = np.max( problem_params[2:4] )
	temp[2] = np.max( problem_params[0:2] )
	temp[3] = np.min( problem_params[2:4] )

	problem_params = temp

	return problem_params

def parse_commandline():
	parser = argparse.ArgumentParser(description='High fidelity solver')
	parser.add_argument('-a','--action',help='Action', required=True)
	parser.add_argument('-n','--num-terms',help='Number of KL expansion terms', required=True)
	parser.add_argument('-s','--save',help='Save the solution and the random field', action='store_true', required=False)

	args = parser.parse_args()
	return args

if __name__ == '__main__':

	args = parse_commandline()

	if(args.action == 'continuous'):
		model_params = {}
		model_params['gaus_rand_field'] = np.random.normal(0,1,int(args.num_terms) )
		field_mean = 2.
		model_params['gaus_mean'] = field_mean

	if(args.action == 'two_lines'):
		model_params = {}
		model_params['gaus_rand_field'] = np.random.normal(0,1,int(args.num_terms) )
		field_mean = np.zeros(4)
		field_mean[0] = 2.
		field_mean[1] = 4.
		field_mean[2] = 6.
		field_mean[3] = 8.
		model_params['gaus_mean'] = field_mean

		model_params['line_params'] = generate_model_params()

	if(args.action == 'line'):
		model_params = {}
		model_params['gaus_rand_field'] = np.random.normal(0,1,int(args.num_terms) )
		field_mean = np.zeros(4)
		field_mean[0] = 2.
		field_mean[1] = 4.
		model_params['gaus_mean'] = field_mean

		model_params['line_params'] = np.random.uniform(0.1, 0.9, 2)

	if(args.action == 'blob'):
		model_params = {}
		model_params['gaus_rand_field'] = np.random.normal(0,1,int(args.num_terms) )
		field_mean = np.zeros(4)
		field_mean[0] = 2.
		field_mean[1] = 4.
		model_params['gaus_mean'] = field_mean

		model_params['blob_center'] = np.random.uniform(3/8,5/8,2)
		model_params['off_circle'] = np.random.uniform(1/8,1/4,1)

	high_fid = high_fidelity(args.action)
	high_fid.set_params(model_params)

	xs = np.zeros(2)
	xs[0] = 0.5
	xs[1] = 0.5

	high_fid.set_source_loc(xs)

	high_fid.assemble_bilinear_matrix()
	high_fid.assemble_right_hand_side()

	high_fid.solve()

	if(args.save == True):
		high_fid.save_random_field()
		high_fid.save_solution()
#
#	high_fid.save_from_vec(sol)
